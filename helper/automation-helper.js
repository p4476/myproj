"use strict";

const axios = require("axios");
const fs = require("fs");
const { GoogleAuth } = require("google-auth-library");
const { BigQuery } = require('@google-cloud/bigquery');
const config = require("../config/index")

const bigqueryClient = new BigQuery();

const getSheetData = async (spreadsheetId, range) => {
	let token = await generateTokenForGoogleSheets();
	let url = `https://sheets.googleapis.com/v4/spreadsheets/${spreadsheetId}/values/${range}`;
	let res = await axios.get(url, { headers: { "Authorization": `Bearer ${token.access_token}` } }).catch(err => console.log(err.response));
	return res.data.values;
};

const getSheetList = async (spreadsheetId) => {
	let token = await generateTokenForGoogleSheets();
	let url = `https://sheets.googleapis.com/v4/spreadsheets/${spreadsheetId}?&fields=sheets.properties`;
	let res = await axios.get(url, { headers: { "Authorization": `Bearer ${token.access_token}` } }).catch(err => console.log(err.response));
	return res.data;
};

const createNewTab = async (spreadsheetId, title) => {
	let token = await generateTokenForGoogleSheets();
	let url = `https://sheets.googleapis.com/v4/spreadsheets/${spreadsheetId}:batchUpdate`;
	let body = {
		"requests": [
			{
				"addSheet": {
					"properties": {
						"title": title
					}
				}
			}
		]
	}
	let res = await axios.post(url, body, { headers: { "Authorization": `Bearer ${token.access_token}` } }).catch(err => console.log(err.response));
	return res.data;
};

const deleteTab = async (spreadsheetId, title) => {
	let token = await generateTokenForGoogleSheets();
	let url = `https://sheets.googleapis.com/v4/spreadsheets/${spreadsheetId}:batchUpdate`;
	let body = {
		"requests": [
			{
				"deleteSheet": {
					"properties": {
						"title": title
					}
				}
			}
		]
	}
	let res = await axios.post(url, body, { headers: { "Authorization": `Bearer ${token.access_token}` } }).catch(err => console.log(err.response));
	return res;
};

const updateTabInSheet = async (spreadsheetId, range, data) => {
	let token = await generateTokenForGoogleSheets();
	let url = `https://sheets.googleapis.com/v4/spreadsheets/${spreadsheetId}/values/${range}?valueInputOption=USER_ENTERED`;
	let body = {
		"values": data
	}
	let res = await axios.put(url, body, { headers: { "Authorization": `Bearer ${token.access_token}` } }).catch(err => console.log(err.response));
	return res.data;
}

const generateTokenForGoogleSheets = async () => {
	let body = {
		"client_id": process.env.clientId,
		"client_secret": process.env.clientSecret,
		"grant_type": "refresh_token",
		"refresh_token": process.env.refreshToken
	};
	let url = "https://oauth2.googleapis.com/token";
	let res = await axios.post(url, body).catch(err => console.log(err.response));
	return res.data;
};

const generateTokenForDialogflow = async () => {
	let auth = new GoogleAuth();
	await auth.getClient().catch(err => console.log(err)).catch(err => console.log(err.response));
	let token = await auth.getAccessToken();
	return token;
};

const detectIntent = async (token, projectId, sessionId, query, languageCode) => {
	let url = `https://dialogflow.googleapis.com/v2beta1/projects/${projectId}/agent/sessions/${sessionId}:detectIntent`;
	let body = {
		"queryInput": {
			"text": {
				"languageCode": languageCode,
				"text": query
			}
		}
	}
	let headers = {
		"Content-Type": "application/json",
		"Authorization": `Bearer ${token}`
	};
	let res = await axios.post(url, body, { headers }).catch(err => console.log(err.response));
	return res.data;
}


const getDBData = async (intent_name, entities) => {
	try {
		let query = null;
		let options = null;

		//Constructing query according to country or world
		if (entities) {
			query = `select chat_response,telephony_response,links,labels
			from ${config[config.run].dataset} 
			where intent_name=@intent_name and entities=@entities`;

			options = {
				query: query,
				location: 'US',
				params: { intent_name: intent_name, entities: entities }
			};
		} else {
			query = `select chat_response,telephony_response,links,labels
			from ${config[config.run].dataset} 
			where intent_name=@intent_name and entities is null`;

			options = {
				query: query,
				location: 'US',
				params: { intent_name: intent_name }
			};
		}
 
		return bigqueryClient.query(options);

	} catch (err) {
		console.log(err)
	}

}


const setPivotTable = async (statsSheetId,resultSheetId,lastRow) => {
	const requests = [
		{
			updateCells: {
				rows: {
					values: [
						{
							pivotTable: {
								source: {
									sheetId: resultSheetId,
									startRowIndex: 0,
									endRowIndex: lastRow,
									startColumnIndex: 0,
									endColumnIndex: 28
								},
								rows: [
									{
										sourceColumnOffset: 4,
										showTotals: true,
										sortOrder: "ASCENDING"
									}
								],
								columns: [
									{
										sourceColumnOffset: 20,
										showTotals: true,
										sortOrder: "ASCENDING"
									}
								],
								values: [
									{
										sourceColumnOffset: 20,
										summarizeFunction: "COUNTA",
										calculatedDisplayType: "PERCENT_OF_ROW_TOTAL"
									},
									{
										sourceColumnOffset: 20,
										summarizeFunction: "COUNTA"
									}
								]
							}
						}
					]
				},
				start: {
					sheetId: statsSheetId,
               		rowIndex: 0,
                	columnIndex: 0
				},
				fields: "pivotTable"
			}
		}
	]

	const body = {
		requests
	};

	let token = await generateTokenForGoogleSheets();
	let url = `https://sheets.googleapis.com/v4/spreadsheets/${config[config.run].sheet}:batchUpdate`;

	let res = await axios.post(url, body, { headers: { "Authorization": `Bearer ${token.access_token}` } }).catch(err => console.log(err.response));
	return res;
}



module.exports = {
	generateTokenForDialogflow,
	generateTokenForGoogleSheets,
	getSheetData,
	detectIntent,
	getSheetList,
	createNewTab,
	deleteTab,
	updateTabInSheet,
	getDBData,
	setPivotTable
}