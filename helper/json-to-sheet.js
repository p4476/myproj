const XLSX = require('xlsx');


const data = require('/home/anamika/Downloads/result.json');

var wb = XLSX.utils.book_new();
wb.Props = {
    Title: "UPENN-9APR",
    Subject: "Test Result",
    Author: "Anamika Gawandi",
    CreatedDate: new Date()
};

wb.SheetNames.push("result");

var ws = XLSX.utils.json_to_sheet(data);

wb.Sheets["result"] = ws;


const fileName = 'UPENN.xlsx';

// XLSX.utils.book_append_sheet(wb, ws, 'results');

XLSX.writeFile(wb, fileName);

