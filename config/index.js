module.exports = {
    run : "govQA",
    ides : {
        projectId : "qp-gcovid-ides-2020-03",
        key : "ides.json",
        sheet : "16fg5SvLwaJ_ZEezHO93mRXzcpiFPYLa9maWN0m0FILw",
        tab : "PUA",
        srcRange : "A2:L10000",
        resultTab : "testingPUA",
        resultRange : "A1:AC10000",
        dataset : 'qp-gcovid-ides-2020-03.ides.cmsQA',
        statsTab : "stats",
        //Text shoud be provided if standard to be shown else null if same response to be shown as text
        telephonyResponseIfBlank : "Sorry, we are not able to answer your query at this moment. Please try after sometime."
    },
    upenn : {
        projectId : "qp-gcovid-ides-2020-03",
        key : "upenn.json",
        sheet : "16fg5SvLwaJ_ZEezHO93mRXzcpiFPYLa9maWN0m0FILw",
        tab : "test",
        srcRange : "A2:L10000",
        resultTab : "testing-result",
        resultRange : "A1:AC10000",
        dataset : 'qp-gcovid-ides-2020-03.ides.cmsv2',
        //Text shoud be provided if standard to be shown else null if same response to be shown as text
        telephonyResponseIfBlank : "Sorry, we are not able to answer your query at this moment. Please try after sometime."
    },
    ota : {
        projectId : "pikepassdotcom-chatbot-test",
        key : "otaQA.json",
        sheet : "1s5WDsOC3Gr4hT32K-G5QZAZXrkraOVfowSn21If5uYo",
        tab : "Regression",
        srcRange : "A2:L10000",
        resultTab : "4JUN-4",
        resultRange : "A1:AC10000",
        dataset : null,
        statsTab : "stats",
        //Text shoud be provided if standard to be shown else null if same response to be shown as text
        telephonyResponseIfBlank : null
    },
    otadev : {
        projectId : "pikepassdotcom-chatbot-dev",
        key : "otaDev.json",
        sheet : "1s5WDsOC3Gr4hT32K-G5QZAZXrkraOVfowSn21If5uYo",
        tab : "Regression",//"OTAQueries",
        srcRange : "A2:L10000",
        resultTab : "Regression-1",
        resultRange : "A1:AC10000",
        dataset : null,
        statsTab : "stats",
        //Text shoud be provided if standard to be shown else null if same response to be shown as text
        telephonyResponseIfBlank : null
    },
    otaprod : {
        projectId : "pikepassdotcom-chatbot-prod",
        key : "otaProd.json",
        sheet : "1s5WDsOC3Gr4hT32K-G5QZAZXrkraOVfowSn21If5uYo",
        tab : "Sheet17",//"OTAQueries",
        srcRange : "A2:L10000",
        resultTab : "Sheet17-2",
        resultRange : "A1:AC10000",
        dataset : null,
        statsTab : "stats",
        //Text shoud be provided if standard to be shown else null if same response to be shown as text
        telephonyResponseIfBlank : null
    },
    summa : {
        projectId : "covid-bot-development",
        key : "summa.json",
        sheet : "10ZOFcFc2f3MzcV--bjNPiL7V3nKlt-U0pzPdD0ih704",
        tab : "summa",
        srcRange : "A2:L10000",
        resultTab : "Test-3",
        resultRange : "A1:AC10000",
        dataset : 'covid-bot-development.summa_cms.cms',
        statsTab : "pivot-T1",
        //Text shoud be provided if standard to be shown else null if same response to be shown as text
        telephonyResponseIfBlank : "Sorry, we are not able to answer your query at this moment. Please try after sometime."
    },
    govQA : {
        projectId : "governor-office-bot-qa",
        key : "governorQA.json",
        sheet : "14-qYkmxn_8vs0AvJaghQ--vqaXt0Mm1CXQccsuSx79w",
        tab : "test",
        srcRange : "A2:L10000",
        resultTab : "4JUN-1",
        resultRange : "A1:AC10000",
        dataset : "governor-office-bot-qa.governor_office_qa_cms.cms_qa_eng",
        statsTab : "stats",
        //Language of the bot. Use 'en-US' for English and 'es' for Spanish
        lang: "es",
        //Text shoud be provided if standard to be shown else null if same response to be shown as text
        telephonyResponseIfBlank : null
    },
};