"use strict";

const uuid = require("uuid/v4");
const automationHelper = require("./helper/automation-helper");
const fs = require("fs");
const config = require("./config/index")
const sleep = require('sleep');
const start = async () => {
	let result = [];
	let tempResult = [];
	result.push(["sessionId", "query", "expChatRes", "expTeleRes", "expIntent", "expEntity", "isWebhook", "expLinks", "expLabels", "expImg", "expImgDescription", "expSuggestionChips", "actChatRes", "actTeleRes", "actIntent", "actEntity", "actIsWebhook", "actLinks", "actImg", "actSuggestionChips", "statusIntent", "statusChat", "statusTelephony", "statusIsWebhook", "statusLinks", "statusImg", "statusSuggestionChips", "DF_Response"])
	let testCases = await automationHelper.getSheetData(config[config.run].sheet, `${config[config.run].tab}!${config[config.run].srcRange}`);
	console.log("fetched data from cms");
	let sessionIdPrefix = uuid();
	for (let i = 0; i < testCases.length; i++) {
		let expIntentList = testCases[i][4].split("$:$");
		let testCaseDetails = {
			sessionId: `${sessionIdPrefix}-${testCases[i][0]}`,
			query: testCases[i][1],
			expChatRes: testCases[i][2],
			expTeleRes: testCases[i][3],
			expIntent: expIntentList,
			expEntity: testCases[i][5],
			expIsWebhook: testCases[i][6],
			expLinks: testCases[i][7],
			expLabels: testCases[i][8],
			expImg: testCases[i][9],
			expImgDescription: testCases[i][10],
			expSuggestionChips: testCases[i][11],
		}
		try {
			let bqData = await automationHelper.getDBData(testCaseDetails.expIntent[0], testCaseDetails.expEntity)
			// console.log(bqData)
			testCaseDetails.expChatRes = bqData[0][0]["chat_response"];
			testCaseDetails.expTeleRes = bqData[0][0]["telephony_response"];
			testCaseDetails.expLinks = bqData[0][0]["links"];
			testCaseDetails.expLabels = bqData[0][0]["labels"];
			// testCaseDetails.expImg = bqData[0][0]["images"],
			// testCaseDetails.expImgDescription = bqData[0][0]["descriptions"]

			// testCaseDetails.expEntity = bqData[0][0]["entities"]
		} catch (err) {
			// console.log(err)
			testCaseDetails.expChatRes = testCases[i][2];
			testCaseDetails.expTeleRes = testCases[i][3];
			testCaseDetails.expLinks = testCases[i][7];
			testCaseDetails.expLabels = testCases[i][8];
			// testCaseDetails.expImg = testCases[i][9];
			// testCaseDetails.expImgDescription = testCases[i][10];
			// testCaseDetails.expEntity = testCases[i][5]
		}
		if ((!testCaseDetails.expTeleRes) && config[config.run].telephonyResponseIfBlank) {
			testCaseDetails.expTeleRes = config[config.run].telephonyResponseIfBlank
		}
		let testCaseResult = await executeTestCase(testCaseDetails);
		let testCaseResultInArray = [];
		Object.keys(testCaseResult).forEach(key => {
			testCaseResultInArray.push(testCaseResult[key])
		})
		result.push(testCaseResultInArray);
		tempResult.push(testCaseResult);
		console.log(`Completed test case - ${i} / ${testCases.length}`);
		console.log(`Status of  case - ${testCaseResult["statusChat"]}`);
		// sleep.msleep("500");
	}
	// await fs.writeFileSync("./result.json", JSON.stringify(result));
	await storeResult(result, tempResult, testCases.length);
	console.log("complete");
};

const executeTestCase = async (testCaseDetails) => {
	let result = {
		actChatRes: "",
		actTeleRes: "",
		actIntent: "",
		actEntity: "",
		actIsWebhook: "",
		actLinks: "",
		actImg: "",
		actSuggestionChips: "",
		statusIntent: "",
		statusChat: "",
		statusTelephony: "",
		statusIsWebhook: "",
		statusLinks: "",
		statusImg: "",
		statusSuggestionChips: "",
		DF_Response: ""
	}
	let token = await automationHelper.generateTokenForDialogflow();
	try {
		let res = await automationHelper.detectIntent(token, config[config.run].projectId, testCaseDetails.sessionId, testCaseDetails.query, `${config[config.run].lang}`)
		let parsedDfRes = parseDialogflowResponse(res);
		// console.log(result.actChatRes, "  ",parsedDfRes.chatResponse,"   ",result.actChatRes)
		result.actChatRes = parsedDfRes.chatResponse;
		result.actTeleRes = parsedDfRes.telephonyResponse;
		result.actIntent = parsedDfRes.intent;
		result.actEntity = parsedDfRes.parameters;
		result.actIsWebhook = parsedDfRes.isWebhook;
		result.actLinks = JSON.stringify(parsedDfRes.links);
		result.actImg = JSON.stringify(parsedDfRes.img);
		result.actSuggestionChips = JSON.stringify(parsedDfRes.suggestionChips);

		//Call function which compare payloads
		// let payloadResponses = await Promise.all([

		result.statusLinks = compareLinks(testCaseDetails.expLinks, testCaseDetails.expLabels, parsedDfRes.links);
		result.statusImg = compareImg(testCaseDetails.expImg, testCaseDetails.expImgDescription, parsedDfRes.img);
		result.statusSuggestionChips = compareSuggestionChips(testCaseDetails.expSuggestionChips, parsedDfRes.suggestionChips);

		// console.log("Type of Links", typeof result.statusLinks, "\n", result.statusLinks)
		// console.log("payload resp", payloadResponses);

		if ((testCaseDetails.expIsWebhook && testCaseDetails.expIsWebhook === "TRUE" && result.actIsWebhook === "Webhook execution successful") || (testCaseDetails.expIsWebhook && testCaseDetails.expIsWebhook === "FALSE" && result.actIsWebhook === "Unsuccessful")) {
			result.statusIsWebhook = "PASS"
		} else {
			result.statusIsWebhook = "FAIL"
		}
		if (testCaseDetails.expIntent.includes(result.actIntent)) {
			result.statusIntent = "PASS"
		} else {
			result.statusIntent = "FAIL"
		}
		testCaseDetails.expIntent = testCaseDetails.expIntent.join("$:$");
		if (testCaseDetails.expChatRes === result.actChatRes) {
			result.statusChat = "PASS"
		} else {
			result.statusChat = "FAIL"
		}
		if (testCaseDetails.expTeleRes === result.actTeleRes) {
			result.statusTelephony = "PASS"
		} else {
			result.statusTelephony = "FAIL"
		}
		if (result.statusChat === "FAIL" || result.statusTelephony === "FAIL" || result.statusIntent === "FAIL" || result.statusIsWebhook === "FAIL"
			|| result.statusLinks === "FAIL" || result.statusImg === "FAIL" || result.statusSuggestionChips === "FAIL") {
			result.DF_Response = JSON.stringify(res);
		}
	} catch (err) {
		result.statusIntent = "SKIPPED";
		result.statusChat = "SKIPPED";
		result.statusTelephony = "SKIPPED";
		result.statusIsWebhook = "SKIPPED";
		result.statusLinks = "SKIPPED";
		result.statusImg = "SKIPPED";
		result.statusSuggestionChips = "SKIPPED";
		result.DF_Response = JSON.stringify(err);
		console.log(err);
	}
	// console.log("testcases",testCaseDetails)
	// console.log("result",result)
	return { ...testCaseDetails, ...result };
};

const compareLinks = (links, labels, actualLinks) => {
	// return new Promise((resolve, reject) => {
	try {
		if (!links && !labels && actualLinks.length == 0)
			return "PASS";
		else if (links && labels && actualLinks.length == 0) {
			return "FAIL";
		} else if (!links && !labels && actualLinks.length > 0) {
			return "FAIL";
		} else if ((!links && labels) || (links && !labels)) {
			return "FAIL";
		} else if (links && labels && actualLinks.length > 0) {
			let linkArr = links.split("$:$");
			let labelArr = labels.split("$:$");

			// console.log("Length",linkArr.length, labelArr.length , actualLinks.length)

			if (!(linkArr.length === labelArr.length && linkArr.length === actualLinks.length)) {
				// console.log("in length matching loop")
				return "FAIL";
			}

			let expLinks = [];
			for (let i = 0; i < linkArr.length; i++) {
				let obj = {};
				obj[linkArr[i]] = null;
				obj[linkArr[i]] = labelArr[i];
				expLinks.push(obj);
			}
			expLinks = expLinks.sort();

			let actLinks = [];
			for (let i = 0; i < actualLinks.length; i++) {
				let obj = {};
				obj[actualLinks[i].link] = null;
				obj[actualLinks[i].link] = actualLinks[i].text;
				actLinks.push(obj);
			}
			actLinks = actLinks.sort();
			// actLinks["test"]='abc';
			// console.log("Links\n", JSON.stringify(expLinks), "\n", JSON.stringify(actLinks));
			// console.log("matching links", JSON.stringify(expLinks) !== JSON.stringify(actLinks))

			if (JSON.stringify(expLinks) !== JSON.stringify(actLinks)) {
				// console.log("in last loop")
				return "FAIL";
			}

			return "PASS";
		} else
			return "FAIL";
	} catch (err) {
		console.log(err);
	}

	// })
}

const compareImg = (img, description, actualImgs) => {
	// return new Promise((resolve, reject) => {
	try {
		if (!img && !description && actualImgs.length == 0)
			return "PASS";
		else if (img && description && actualImgs.length == 0) {
			return "FAIL";
		} else if (!img && !description && actualImgs.length > 0) {
			return "FAIL";
		} else if ((!img && description) || (img && !description)) {
			return "FAIL";
		} else if (img && description && actualImgs.length > 0) {
			let imgArr = img.split("$:$");
			let descriptionArr = description.split("$:$");

			if (!(imgArr.length === descriptionArr.length && imgArr.length === actualImgs.length))
				return "FAIL";

			let expImgs = [];
			for (let i = 0; i < imgArr.length; i++) {
				let obj = {};
				obj[imgArr[i]] = null;
				obj[imgArr[i]] = descriptionArr[i];
				expImgs.push(obj);
			}
			expImgs = expImgs.sort();

			let actImgs = [];
			for (let i = 0; i < actualImgs.length; i++) {
				let obj = {};
				obj[actualImgs[i].rawUrl] = null;
				obj[actualImgs[i].rawUrl] = actualImgs[i].accessibilityText;
				actImgs.push(obj);
			}
			actImgs = actImgs.sort();
			// console.log("Imgs", expImgs, "\n", actImgs);
			// console.log(JSON.stringify(expImgs));
			// console.log(JSON.stringify(actImgs));

			if (JSON.stringify(expImgs) !== JSON.stringify(actImgs))
				return "FAIL";

			return "PASS";
		} else
			return "FAIL";

	} catch (err) {
		console.log(err);
	}

	// })
}

const compareSuggestionChips = (suggestionChips, actualSuggestionChips) => {
	// return new Promise((resolve, reject) => {
	try {
		if (!suggestionChips && actualSuggestionChips.length == 0)
			return "PASS";
		else if ((!suggestionChips && actualSuggestionChips.length > 0) || (suggestionChips && actualSuggestionChips.length == 0)) {
			return "FAIL";
		} else if (suggestionChips && actualSuggestionChips.length > 0) {
			let suggestionChipsArr = suggestionChips.split("$:$");
			for (let i = 0; i < suggestionChipsArr.length; i++) {
				let value = actualSuggestionChips[0]["options"].filter((element) => {
					return element["text"] === suggestionChipsArr[i];
				});
				if (!value[0])
					return "FAIL";
			}
			return "PASS";
		} else
			return "FAIL";

	} catch (err) {
		console.log(err);
	}

	// })
}

const parseDialogflowResponse = (dfResponse) => {
	let res = {
		intent: dfResponse.queryResult.intent.displayName,
		parameters: JSON.stringify(dfResponse.queryResult.parameters) || "NA",
		chatResponse: dfResponse.queryResult.fulfillmentText,
		telephonyResponse: dfResponse.queryResult.fulfillmentText,
		isWebhook: "Unsuccessful",
		suggestionChips: [],
		links: [],
		img: []
	}
	if (dfResponse.webhookStatus && dfResponse.webhookStatus.message)
		res.isWebhook = dfResponse.webhookStatus.message
	for (let i = 0; i < dfResponse.queryResult.fulfillmentMessages.length; i++) {
		if (dfResponse.queryResult.fulfillmentMessages[i].platform && dfResponse.queryResult.fulfillmentMessages[i].platform === "TELEPHONY" && dfResponse.queryResult.fulfillmentMessages[i].telephonySynthesizeSpeech) {
			if (dfResponse.queryResult.fulfillmentMessages[i].telephonySynthesizeSpeech.ssml) {
				res["telephonyResponse"] = dfResponse.queryResult.fulfillmentMessages[i].telephonySynthesizeSpeech.ssml;
			} else if (dfResponse.queryResult.fulfillmentMessages[i].telephonySynthesizeSpeech.text) {
				res["telephonyResponse"] = dfResponse.queryResult.fulfillmentMessages[i].telephonySynthesizeSpeech.text;
			}
		}
		if (dfResponse.queryResult.fulfillmentMessages[i].payload && dfResponse.queryResult.fulfillmentMessages[i].payload.richContent && dfResponse.queryResult.fulfillmentMessages[i].payload.richContent[0][0].type === "chips") {
			res.suggestionChips.push(dfResponse.queryResult.fulfillmentMessages[i].payload.richContent[0][0])
		}
		if (dfResponse.queryResult.fulfillmentMessages[i].payload && dfResponse.queryResult.fulfillmentMessages[i].payload.richContent && dfResponse.queryResult.fulfillmentMessages[i].payload.richContent[0][0].type === "image") {
			res.img.push(dfResponse.queryResult.fulfillmentMessages[i].payload.richContent[0][0])
		}
		if (dfResponse.queryResult.fulfillmentMessages[i].payload && dfResponse.queryResult.fulfillmentMessages[i].payload.richContent && dfResponse.queryResult.fulfillmentMessages[i].payload.richContent[0][0].type === "button") {
			res.links.push(dfResponse.queryResult.fulfillmentMessages[i].payload.richContent[0][0])
		}
	}
	return res;
}

const storeResult = async (result, tempResult, length) => {
	let tabName = config[config.run].resultTab
	try {
		let newTab = await automationHelper.createNewTab(config[config.run].sheet, tabName);
		await automationHelper.updateTabInSheet(config[config.run].sheet, `${tabName}!${config[config.run].resultRange}`, result);
		const resultSheetId = newTab.replies[0].addSheet.properties.sheetId;
		let statsSheetId = null;
		let res = await automationHelper.getSheetList(config[config.run].sheet);
		for (let i = 0; i < res.sheets.length; i++) {
			if (res.sheets[i].properties.title === config[config.run].statsTab)
				statsSheetId = res.sheets[i].properties.sheetId;
		}
		if (!statsSheetId) {
			let statsTab = await automationHelper.createNewTab(config[config.run].sheet, config[config.run].statsTab);
			statsSheetId = statsTab.replies[0].addSheet.properties.sheetId;
		}
		await automationHelper.setPivotTable(statsSheetId, resultSheetId, length + 1);
	} catch (err) {
		console.log(err);
		await fs.writeFileSync("./result.json", JSON.stringify(tempResult));
	}

}

start();
